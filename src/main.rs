use num::Unsigned;
use std::time::Instant;
use std::time::Duration;
use std::fmt::Display;
use std::ops::BitAnd;
use num::PrimInt;
use mod_exp::mod_exp;
//use rand::prelude::*;

pub fn generic_extended_euclid<U>(a:U,b:U)->(U,U,U)
where
U: Unsigned + PartialOrd + Copy + std::ops::BitAnd,
{
    let one:U = U::one();
    let zero:U = U::zero();
    let two:U = one+one;
    let mut q:U;
    let ( mut r_temp, mut u_temp, mut v_temp):(U,U,U);
    let (mut u_1,mut v_1, mut u_2, mut v_2, mut r_1, mut r_2):(U,U,U,U,U,U)=(one,zero,zero,one,a,b);
    let mut n:U = zero;
    while !r_2.is_zero(){
        q=r_1/r_2;
        (r_temp,u_temp,v_temp) = (r_1,u_1,v_1);
        (r_1,u_1,v_1)=(r_2,u_2,v_2);
        if r_temp>=q*r_2{
            r_2=r_temp-q*r_2;    
        }else{
            r_2=q*r_2-r_temp;
        }
        (u_2,v_2,n)= (u_temp+q*u_2,v_temp+q*v_2,n + one);
    }
    if (n%two).is_one(){
        u_1=b-u_1;
    }else{
        v_1=a-v_1
    }
    return (r_1,u_1,v_1);
}


pub fn inverse_mod<U>(n:U,modulus:U)->U 
where
U: Unsigned + PartialOrd + Copy + std::ops::BitAnd,
{
    //to do: proper error handling
    let inv:U;
    println!("searching for the inverse");
    let result = generic_extended_euclid(modulus,n);
    println!("inverse found");
    if result.0.is_one() 
    {
        inv = result.2;
        return inv;
    }else
    {
        panic!("element non inversible");
    }
}

/* 
pub fn miller_rabin<U>(n:U)->(U,U,U)
where U: Unsigned + PartialOrd + Copy +Display + std::ops::BitAnd,
{
    let one:U = U::one();
    let zero:U = U::zero();
    let two:U = one+one;
    let n_minus_one = n - one;
    // chose random number a < n
    // decompose n-1= d*2^s


    //try a^d
    //if a^d != +- 1 mod n and if a^(d*2^r) != +- 1 for 

}
// */
pub fn biggest_g_power_of_two<U>(g:U,v:U)->(U,U,U)
where U: Unsigned + PartialOrd + Copy +Display + std::ops::BitAnd,
{  
    let one:U=U::one();
    let mut g_past:U;
    let mut g_temp :U = g;
    let mut exp :U = one;
    let two = one+one;
    if g < v
    {
        while 
        {   
            g_past=g_temp;
            g_temp=g_temp*g_temp;
            exp = two*exp;
            g_temp < v
        }{}
    }else{
        (g_temp,g_past,exp)=(U::one(),U::one(),U::zero());
    }
    return (g_temp,g_past,exp);
} 


//* 
pub fn find_power<U>( g:U, max:U)->(U,U)
where U: Unsigned + PartialOrd + Copy + Display+ std::ops::BitAnd, 
{
    let one :U = U::one();
    let zero:U = U::zero();
    let two:U= one + one;
    let mut numb:U=one;
    let mut pow:U=zero;
    while numb < max
    {
        numb = numb*g;
        pow = pow +one;
    }
    if numb==max
    {
        return (numb,pow);
    }else
    {
        return (numb/g,pow-one);        
    }
}

// */
/* 
pub fn find_power<U>( g:U, max:U)->(U,U)
where U: Unsigned + PartialOrd + Copy + Display+ std::ops::BitAnd, 
{
    let one :U = U::one();
    let zero:U = U::zero();

    if g > max
    {
        println!("numb {} pow {} g< max",one, zero);
     
        return ( one, zero);
    }else
    {
        let two:U= one + one;
        let mut numb:U=g;
        let mut numb_temp=g;
        let mut numb_aux;
        
        let mut pow:U=one;
        let mut pow_temp=one;
        let mut pow_aux;
        
        let mut round_count=zero; 
        while numb < max 
        {
            numb_temp=numb;
            pow_temp = pow;
            numb = numb*numb;
            pow = pow + pow;
            round_count = round_count+one;
        }
        numb = numb_temp;
        pow  = pow_temp;
        while round_count > zero
        {
            numb_aux=g;
            pow_aux=one;
            while numb*numb_aux < max && numb_temp*numb_temp < numb
            {
                numb_temp=numb_aux;
                pow_temp = pow_aux;
                numb_aux = numb_aux*numb_aux;
                pow_aux = pow_aux + pow_aux;
            } 
            if numb*numb_temp <= max
            {
                numb = numb*numb_temp;
                pow = pow + pow_temp;    
            }
            round_count = round_count-one;
        }
        println!("numb {} pow {}",numb, pow);
        return (numb,pow);
        
    }
 }

// */
/* 
pub fn max_parameters<U>(g:U,modulus:U)->(U,U,U,U,U,U,U,U)
where 
U: Unsigned + PartialOrd + Copy + Display,
{
    let one :U = U::one();
    let zero:U = U::zero();
    let two = one + one;
    let (g_max,pow_max)=find_power(g,modulus);
    let q_max:U = modulus/g_max;
    
    let q:U = modulus/g;
    let r_g_max :U = modulus -g_max*q_max;
    let r_g:U = modulus -g*q;

    let inv_rg_max:U= inverse_mod(r_g_max,g_max);
    let inv_rg:U= inverse_mod(r_g,g);

    return (g_max,pow_max,q_max,q,r_g_max,r_g,inv_rg_max,inv_rg);
}
// */

pub fn max_parameters<U>(g:U,modulus:U)->(U,U,U,U,U,U,U,U,U,U,U,U,U,U,U,U)
where 
U: Unsigned + PartialOrd + Copy + Display+ std::ops::BitAnd,
{
    let one :U = U::one();
    let zero:U = U::zero();
    let two:U = one + one;
    let three:U = two + one;
    let five:U=three+two;
    let seven:U=five+two;
    let (mut g_max,mut pow_max):(U,U)=find_power(g,modulus);
    let (mut g_n_3,mut pow_n_3):(U,U)=find_power(g,modulus*modulus*modulus);
    let (mut g_n_2,mut pow_n_2):(U,U)=find_power(g,modulus*modulus);
    
    g_n_3=g_n_3/g;
    pow_n_3= pow_n_3 -one;
    
    //g_n_3=g_n_3*g;
    //pow_n_3= pow_n_3 +one;
    println!("pow_n_2: {}",pow_n_2);
    
    //* 
    
    g_n_2=g_n_2*g*g*g;
    pow_n_2= pow_n_2 + one + one + one;
    
    // */
    
    //g_n_2=g_n_2*g;
    //pow_n_2= pow_n_2 + one ;
    
    g_max= one;
    pow_max=zero;
    for i in 0..21
    {
        g_max = g_max*g;
        pow_max = pow_max+one;
    }

    let q_max:U = modulus/g_max; 
    let q:U = modulus/g;
    let r_g_max :U = modulus -g_max*q_max;
    let r_g:U = modulus - g*q;
    let q_3:U = g_n_3/modulus;
    let r_g_3:U= g_n_3 - q_3*modulus;

    let inv_rg_max:U= inverse_mod(r_g_max,g_max);
    let inv_rg:U= inverse_mod(r_g,g);
    let inv_modulus_3:U = inverse_mod(modulus,g_n_3);
    let inv_modulus_2:U = inverse_mod(modulus,g_n_2);

    return (g_max,pow_max,q_max,q,r_g_max,r_g,inv_rg_max,inv_rg,inv_modulus_3,g_n_3,pow_n_3,inv_modulus_2,g_n_2,pow_n_2,q_3,r_g_3);
}



//* 
pub fn discrete_log_rem<U>(g:U,r:U,modulus:U,g_n_3:U,pow_n_3:U,inv_modulus_3:U)->(U,U,U)
where 
U: Unsigned + PartialOrd + Copy + Display + std::ops::BitAnd<Output = U> + std::ops::Shr<Output = U> + std::ops::Shl<Output = U> + PrimInt, <U as BitAnd>::Output: PartialEq<U>
{

    let one :U = U::one();
    let zero:U = U::zero();

    let mut k:U;
    let mut v_temp:U= r;
    let mut v_aux:U = v_temp;
    let mut a:U = zero;
    let mut c=zero;
    let mut c_2=zero;
    let mask_3 = g_n_3-one;
    let mut m = zero;
    //* 
    if (v_temp&(v_temp-one)).is_zero()
    {

        //println!("r is a power of 2:{}",v_temp);
        while (v_temp & one) == zero
        {

            a = a + one;
            v_temp = v_temp>>one;
            c_2 = c_2 +one;
        
        }
        //println!("v_temp after all divisions:{}",v_temp);
    }
    // */
    while !v_temp.is_one()
    {   
            

            c=c+one;         
          
            k = ((g_n_3-(v_temp&mask_3))*inv_modulus_3)&mask_3;
            
            v_aux = v_temp;
            v_temp = (v_temp +k*modulus)>>pow_n_3;
            a = a + pow_n_3;
               

            //* 
            if (v_temp&(v_temp-one)).is_zero()
            {
                //println!("power of 2, 1");
                while (v_temp & one) == zero
                {

                    a = a + one;
                    v_temp = v_temp>>one;
                    c_2 = c_2 +one;
                
                }
            }else if  (v_aux&one).is_zero()  && (v_temp & one).is_zero() // need to search more for that  
            {
                a = a + one;
                v_temp = v_temp>>one;
                c_2 = c_2 +one;                   
                
            }
            // */
            
    }

    a= a%(modulus-one);
    return (a,c,c_2);

}
// */

//* 
pub fn discrete_log_g_n_3<U>(g:U,r:U,modulus:U,g_n_3:U,g_n_2:U,g_min:U,pow_n_3:U,pow_n_2:U,pow_min:U,q_min:U,q:U,r_g_min:U,inv_modulus_3:U,inv_modulus_2:U,inv_rg_min:U,inv_rg:U,r_g:U)->(U,U,U)
where 
U: Unsigned + PartialOrd + Copy + Display + std::ops::BitAnd<Output = U> + std::ops::Shr<Output = U> + std::ops::Shl<Output = U> + PrimInt, <U as BitAnd>::Output: PartialEq<U>
{

    let one :U = U::one();
    let zero:U = U::zero();

    let mut k:U;
    let mut v_temp:U= r;
    let mut v_aux:U = v_temp;
    let mut a:U = zero;
    let mut c=zero;
    let mut c_2=zero;
    let mask_3 = g_n_3-one;
    let mask_2 = g_n_2-one;
    let mask_min = g_min-one;
    let mut m = zero;
    /* 
    if (v_temp&(v_temp-one)).is_zero()
    {

        //println!("r is a power of 2:{}",v_temp);
        while (v_temp & one) == zero
        {

            a = a + one;
            v_temp = v_temp>>one;
            c_2 = c_2 +one;
        
        }
        println!("v_temp after all divisions:{}",v_temp);
    }
    // */
    while !v_temp.is_one()
    //while  !(v_temp&(v_temp-one)).is_zero() 
    {   
            

            c=c+one;         
            
            /* 
            k = ((g_n_3-(v_temp&mask_3))*inv_modulus_3)&mask_3;
            v_aux = (v_temp+k*modulus);
            v_temp = v_aux>>pow_n_3;       
            a = a + pow_n_3;
            // */
            
            
            //* 
            k = ((g_n_2-(v_temp&mask_2))*inv_modulus_2)&mask_2;
            v_aux = (v_temp+k*modulus);
            v_temp = v_aux>>pow_n_2;
            a = a + pow_n_2;
            // */
            
            /* 
            k = ((g_min-(v_temp&mask_min))*inv_rg_min)&mask_min;
            v_aux = (v_temp+k*modulus);
            v_temp = v_aux>>pow_min;
            a = a + pow_min;
            // */
            
            
            
            //* 
            if (v_temp&(v_temp-one)).is_zero()
            {
                //println!("power of 2, 1");
                while (v_temp & one) == zero
                {

                    a = a + one;
                    v_temp = v_temp>>one;
                    c_2 = c_2 +one;
                
                }
            }else if (v_temp & one) == zero 
            {
                a = a + one;
                v_temp = v_temp>>one;
                c_2 = c_2 +one;                   
                
            }
            // */
            
    }
        
    a= a%(modulus-one);

    return (a,c,c_2);

}
// */


pub fn discrete_log_n<U>(g:U,r:U,g_max:U,pow_max:U,q_max:U,r_g_max:U,inv_rg_max:U)->U
where 
U: Unsigned + PartialOrd + Copy + Display + std::ops::BitAnd,
{
    let one :U = U::one();
    let zero:U = U::zero();


    let mut k:U;
    let mut v_temp:U=r;
    let mut a:U = zero;
    while !v_temp.is_one() 
    {   

        k=( (g_max-(v_temp%g_max))*inv_rg_max)%g_max;
        v_temp =(v_temp+k*r_g_max)/g_max + k*q_max;
        a = a + pow_max;
 
        while (v_temp%g).is_zero()
        {
            a = a + one;
            v_temp = v_temp/g;
            //println!("v_temp%g in :{}",v_temp%g);
            //println!("a{}",a);
        }        
    }
    //println!("loop_counter:{}",loop_counter);
    return a;

}



fn main() {

    
    //let modulus_2:u128 =modulus*modulus*modulus;
    //let g:u128=911;
    //let g:u128=5;
    let g:u128=2;
    let mut r:u128;
    let mut x:u128;
    let mut c:u128;
    let mut c_2:u128;
    let mut vec_pair:Vec<(u128,u128,f64)>=vec![(0,0,0.0);0];
    let mut vec_pair_rem:Vec<(u128,u128,f64)>=vec![(0,0,0.0);0];
    
    
    //* 
    let mut start_time_n_3;
    let mut elapsed_time_n_3=Duration::ZERO;
    let mut elapsed_time_temp=Duration::ZERO;
    // */
    
    //* 
    let mut start_time_rem;
    let mut elapsed_time_rem=Duration::ZERO;
    let mut elapsed_time_rem_temp=Duration::ZERO;
    // */
    
   
    //let mut modulus:u128 =1048343;
    
    /*
    let (g_max,pow_max,q_max,q,r_g_max,r_g,inv_rg_max,inv_rg)=max_parameters(g,modulus);
    
    start_time_n = Instant::now();
    
     
    for i in 1047342..1048343{
    
    r=i as u128;
    
            x= discrete_log_n(g,r,modulus,g_max,pow_max,q_max,q,r_g_max,r_g,inv_rg_max,inv_rg) ;
     
    }
    
    elapsed_time_n += start_time_n.elapsed();
    println!("Average Elapsed time n for modulus {:?} : {:?}",modulus, elapsed_time_n.as_secs_f64()/1000.);

    // */

    //modulus = 2698727;
    let modulus = 2697803;

    let (g_max,pow_max,q_max,q,r_g_max,r_g,inv_rg_max,inv_rg,inv_modulus_3,g_n_3,pow_n_3,inv_modulus_2,g_n_2,pow_n_2,q_3,r_g_3)=max_parameters(g,modulus);

    

    //*     
    //for i in 2697664..2697665{
    for i in (modulus-10001)..modulus{  
    //for i in (modulus-1)..modulus{    
    //for i in 3..modulus{    
        r=i as u128;
        //*        
        start_time_n_3 = Instant::now();    
        (x,c,c_2)= discrete_log_g_n_3(g,r,modulus,g_n_3,g_n_2,g_max,pow_n_3,pow_n_2,pow_max,q_max,q,r_g_max,inv_modulus_3,inv_modulus_2,inv_rg_max,inv_rg,r_g) ;
        elapsed_time_n_3 += start_time_n_3.elapsed();
        elapsed_time_temp = elapsed_time_n_3-elapsed_time_temp;
        println!("{}^{} = {} [{}] ",g,x,r,modulus);
        println!("compteur = {} ",c);
        println!("compteur 2= {} ",c_2);
        // block 63
        /* 
        println!("c*63+c_2 = {} ",63*c+c_2);
        println!("c*63+c_2%(modulus-1)) = {} ",(63*c+c_2)%(modulus-1));
        assert_eq!((63*c+c_2)%(modulus-1),x);
        // */
        // block 45
        
        //* 
        println!("c*45+c_2 = {} ",45*c+c_2);
        println!("c*45+c_2%(modulus-1)) = {} ",(45*c+c_2)%(modulus-1));
        assert_eq!((45*c+c_2)%(modulus-1),x);
        // */

        // block 21
        /* 
        println!("c*21+c_2 = {} ",21*c+c_2);
        println!("c*21+c_2%(modulus-1)) = {} ",(21*c+c_2)%(modulus-1));
        assert_eq!((21*c+c_2)%(modulus-1),x);
        // */

        // block 11
        /* 
        println!("c*11+c_2 = {} ",11*c+c_2);
        println!("c*11+c_2%(modulus-1)) = {} ",(11*c+c_2)%(modulus-1));
        assert_eq!((11*c+c_2)%(modulus-1),x);
        // */
        
        println!("x/compteur = {} ",x/c);
        println!("x%compteur = {} ",x%c);
        //vec_pair.push((x,r,elapsed_time_temp.as_secs_f64()));
        vec_pair.push((x,r,(c+c_2) as f64));
        println!("Elapsed time temp: {:?}",elapsed_time_temp);
        elapsed_time_temp=elapsed_time_n_3;
        assert_eq!(mod_exp(g,x,modulus),r);
        // */
        //*        
        start_time_rem = Instant::now();    
        (x,c,c_2)= discrete_log_rem(g,r,modulus,g_n_3,pow_n_3,inv_modulus_3);
        elapsed_time_rem += start_time_rem.elapsed();
        elapsed_time_rem_temp = elapsed_time_rem-elapsed_time_rem_temp;
        println!("{}^{} = {} [{}] ",g,x,r,modulus);
        println!("compteur = {} ",c);
        println!("compteur 2= {} ",c_2);
        println!("c*63+c_2 = {} ",63*c+c_2);
        println!("c*63+c_2%(modulus-1)) = {} ",(63*c+c_2)%(modulus-1));
        //assert_eq!((63*c+c_2)%(modulus-1),x);
        //println!("c*63+c_2 = {} ",63*c+c_2);
        //assert_ne!(63*c+c_2,x);
        println!("x/compteur = {} ",x/c);
        println!("x%compteur = {} ",x%c);
        //vec_pair.push((x,r,elapsed_time_temp.as_secs_f64()));
        vec_pair_rem.push((x,r,(c+c_2) as f64));
        println!("Elapsed time rem temp: {:?}",elapsed_time_rem_temp);
        elapsed_time_rem_temp=elapsed_time_rem;
        assert_eq!(mod_exp(g,x,modulus),r);
        // */    
        
    
    }
    // */

    //* 
    vec_pair.sort_by(|a,b| a.0.cmp(&b.0));
    vec_pair_rem.sort_by(|a,b| a.0.cmp(&b.0));

    for p in vec_pair.iter().zip(vec_pair_rem.iter())
    {
        let (pair,pair_rem) = p; 
        print!("2^{}={}",pair.0,pair.1);
        let mut i:usize = 0;
        let j = ((pair.2)/1000.0).round() as usize;
        //let j = ((pair.2)*2000.0).round() as usize;
        while i < j
        {
            print!("o");
            i+=1;
        }
        println!();

        print!("2^{}={}",pair_rem.0,pair_rem.1);
        let mut i:usize = 0;
        let j = ((pair_rem.2)/1000.0).round() as usize;
        //let j = ((pair_rem.2)*2000.0).round() as usize;
        while i < j
        {
            print!("*");
            i+=1;
        }
        println!();
    
    }

    println!("Average Elapsed time n_3 for modulus {:?} : {:?}",modulus, elapsed_time_n_3.as_secs_f64()/10000.);
    println!("Average Elapsed time rem for modulus {:?} : {:?}",modulus, elapsed_time_rem.as_secs_f64()/10000.);
    
    // */

    
    

}
