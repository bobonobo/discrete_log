# discrete_log

To run "cargo run"


## Discrete log algorithm

I am reluctant to disclose that program, but I need to show I am not doing nothing.

I don't know whether the algorithm exists or not, there is a sort of memory free "reversed" baby step giant steps, and a "not memory free reversed baby step giant step". What might be new is it uses a stop condition that makes it a sort of real euclidean division, and a way to perform modular inverse with a useful trick for powers of two because then you can use the usual multiplication in $ \mathbb Z $ and masking instead of modular multiplication. The trick is limited by the maximum size of the power used.

Apparently the condition that $g^a$ is a generator is not sufficient as some generators seem to not work for the memory free algo.

I don't see any real limitation for it besides that there is a constraint on $  q $ .

You can contact me on avril.benoit28@gmail.com

Let's explain the maths behind the trick

Say you have a prime number $ N $, a generator $ g $ of $ \mathbb{Z} / { N\mathbb{Z} }^* $ and an element $ r $ of $ \mathbb{Z} / {N\mathbb{Z}}^* $, ( in our case $ g=2 $ ), then the discrete log problem is to find x such that $ g^x \equiv r \mod N $.
In full real numbers this means that $ g^x = r +kN $ for a $ k $ that might be big.  
Associacitivity of the implication aside, loosely speaking, we have

```math
 g^x = r+KN 
```

```math
N =qg+r_g 
```

```math
g^x = r+KN 
```

```math
\Rightarrow g^x  = r+K(qg+r_g) 
```

```math
\Rightarrow r+Kr_g \equiv 0 \mod g
```

```math
\Rightarrow K \equiv (-r)r_g^{-1} \mod g
```

```math
\Rightarrow K = (-r)r_g^{-1} + pg
```

for an unknown $ p $

let's call

\begin{align*}
k & = (g -r)r_g^{-1} \\
\textrm{we have }\\
0 &\leqslant k  \leqslant g \\
\textrm{and }\\
K &= k + (p + r_g^{-1} )g  \\
\Leftrightarrow
K &\equiv k \mod g\\
\end{align*}

so we can deduce that

\begin{align*}
g &\Big| r +((-r)r_g^{-1}+pg)r_g \forall p\\
g &\Big| r +( k + (p + r_g^{-1} )g)r_g\\
g &\Big| r +kr_g\\
g &\Big| r +Kr_g\\
g &\Big| r +K(qg+r_g)\\
g &\Big| r +KN=g^x\\
\end{align*}

but at the same time 

\begin{align*}
g &\Big| r +kr_g\\
g &\Big| r +kr_g+kqg\\
g &\Big| r +kN\\
\end{align*}

so we can conclude that 

\begin{align*}
r +kN & \equiv r +KN \mod N\\
r +kN & \equiv r +KN \mod g\\
\end{align*}

Since $ g $ and $ N $ are coprime, $ \frac{r+kN}{g} = r'\equiv \frac{r+KN}{g} \mod N \equiv g^{x-1} $
And since there are $ N-1 $ value possible only , there is a one on one correspondance between $ x $ and the number of "divisions" you can make . Alternatively, you can multiply by the inverse of $ g $
instead of dividing. It's a way to perform a multiplication by the inverse of $ g $ without the inverse of $ g $.

You can do something similar using $ g^a $ as long as $ g^a $ is a generator of  $ \mathbb{Z} / { N\mathbb{Z} }^* $ i.e. if 
$ a $ is coprime with $ N - 1 $ . The memory free algorithm doesn't necessarily find the "standard" decomposition into $ x = qa +r $ with $ 0 \leqslant r < a $, but a decomposition. 
Let's call $ pow_{min} $ the biggest power of $ g $ in $ [0,N] $ . If $ x/g^a \geqslant g*pow_{min} $, the algorithm is almost certain to converge in $ [0,N] $. This is because there are $ pow_{min} $ powers of $ g $ in $ [0,N] $, if you divide by $ g $ more than $ pow_{min} $ times, then, the algorithm will necessarily find a power of g in the last run. 
If multiplying by $ g^{-a} $ a number lead to a completely random number, the probability of finding a power of two would be $ \frac{pow_{ min }}{ N } $ , so the probability of not finding a power of two each time you multiply by the inverse of $ g^a $ would be  $ 1 -\frac{pow_{ min }}{ N } $, and thus  the probability of not finding a power of $ 2 $ after  $ n $ run  would be  $(1 - \frac{pow_{ min }}{ N })^n $ the argument is not really sound because the algo is not random, it's actually completely deterministic. Taking in account that we never cross by the same value twice as long as we don't do more than $ N-1 $ multiplications, a more realistic probability of not finding a power of $ g $
 would be $ (1 - \frac{pow_{ min }}{ N })(1 - \frac{pow_{ min }+1}{ N })(1 - \frac{pow_{ min }+2}{ N })...(1 - \frac{pow_{ min }+n}{ N }) $.  
In my mind by dividing by g, you somewhat gets closer to powers of g by moving from one cycle to another. A bit like a rho pollard algo, but I can't express it properly. 
You can make it work with $ a $ not being a generator, but you need to be lucky and change the algorithm a bit.
 There are approximately $ x/g^a $ iterations in the algorithm, at each iteration, the probability to get a $ r \mod N $ that can be divided by $ g $, is $ 1/g $. Therefore, on average, after $ n $ iterations of the loop, there will have been $ n/g $ division by $ g $. If there are more divisions than pow_min before $ ng^a > x $, then there will necessarily be a step where $ r' $ will be a power of $ g $.
 